# README #

A market place for parcel delivery by bikers in urban contest.

### Project settings ###
- Disable  Android's Instant Run feature in the Android Studio settings, because it can often interfere with creating the correct set of tables (DBFlow).


### Detailed description ###

Clients (having parcels/packages) to deliver use a mobile app to order delivers. 
The �platform� picks the best courier (based on his location/reputation...) and dispatchs the request, that can be accepted or rejected. 
The delivery should be followd in real time, with tacking options and proof of checkout/check-in.

### Ideas for delivery ensurence ###

- The sender of the package creates with the registration of the package an code. This code is handed over with the package to the courier and the courier has to hande it over to the recipient of the package. The sender and the recipient upload the code so that the code can be compared on a server. If the codes are identical the system knows that package was delivered correctly. The code can be handed over f.e. by scanning an QR-Code. Problems: How to check that the right recipient get the package?

### References ###
- (Youtube PlacePicker)[https://www.youtube.com/watch?v=EF-TovkRYH4]
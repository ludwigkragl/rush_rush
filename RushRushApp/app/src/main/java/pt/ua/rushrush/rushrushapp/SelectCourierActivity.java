package pt.ua.rushrush.rushrushapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SelectCourierActivity extends FragmentActivity implements
        SelectCourierListFragment.OnListItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_courier);
    }

    public void onCourierSelected(Courier courier) {
        String msg = "Courier "+courier.getName()+" selected.";
        Toast.makeText(SelectCourierActivity.this, msg, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, PackageOverviewActivity.class);
        startActivity(i);
    }
}

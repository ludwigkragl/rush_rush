package pt.ua.rushrush.rushrushapp;

/**
 * Created by Luggi on 26.11.2017.
 */
/** @brief First build this class to stay scalable.*/
public class Courier {
    private String name;
    private long stars;
    private String uid; //from firebase registration

    Courier(String n, String u, long s){
        this.name = n;
        this.uid = u;
        this.stars = s;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

    public void setUid(String u){
        this.uid = u;
    }

    public String getUid(){
        return this.uid;
    }

    @Override
    public String toString(){
        return name+" - "+new Long(this.stars).toString()+"*";
    }
}

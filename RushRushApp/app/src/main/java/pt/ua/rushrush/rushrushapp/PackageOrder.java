package pt.ua.rushrush.rushrushapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Ludwig Kragl on 17.11.2017.
 */

/** Class to handle all package order inforomation. */
@Table(database = PackageDatabase.class)
public class PackageOrder extends BaseModel implements Serializable{
    public static final String INTENT_PACKAGE_KEY = "packageOrder";
    LatLng deliveryLocation;
    LatLng pickupLocation;

    @Column
    @PrimaryKey
    String packageName;
    @Column
    Date pickupDate;
    @Column
    double pickupLocationLat;
    @Column
    double pickupLocationLong;
    @Column
    String pickupAddress;
    @Column
    Date deliveryDate;
    @Column
    double deliveryLocationLat;
    @Column
    double deliveryLocationLong;
    @Column
    String deliveryAddress;
    @Column
    String status;
    @Column
    int releaseCourierNumber;
    @Column
    long firebaseId;
    @Column
    int pickupNumber;
    @Column
    String courierUid;

    public PackageOrder(){
        super();
        this.firebaseId = 0;
    }

    public PackageOrder(String name){
        super();
        this.packageName = name;
        this.status = "Order created";
        this.pickupLocation = new LatLng(pickupLocationLat, pickupLocationLong);
        this.deliveryLocation = new LatLng(deliveryLocationLat, deliveryLocationLong);
    }

    public PackageOrder(int cnr, String delivery_address, Date delivery_datetime,
                        LatLng delivery_location, long id, String package_name,
                        String pickup_address, Date pickup_date, LatLng pickup_location,
                        String s){
        super();
        releaseCourierNumber = cnr;
        deliveryAddress = delivery_address;
        deliveryDate = delivery_datetime;
        deliveryLocation = delivery_location;
        firebaseId = id;
        packageName = package_name;
        pickupAddress = pickup_address;
        pickupDate = pickup_date;
        pickupLocation = pickup_location;
        status = s;
    }

    public static ArrayList<PackageOrder> get_examplePackageOrders() {
        ArrayList<PackageOrder> packages = new ArrayList<PackageOrder>();
        packages.add(new PackageOrder("Package 1"));
        packages.add(new PackageOrder("Package 2"));
        packages.add(new PackageOrder("Package 3"));
        return packages;
    }

    String getPackageName(){
        return this.packageName;
    }

    Date getPickupDate() {return this.pickupDate;}

    String getDeliveryAddress(){return this.deliveryAddress;}

    String getPickupAddress(){return this.pickupAddress;}

    Date getDeliverDate() {return this.deliveryDate;}

    LatLng getPickupLocation(){
        this.pickupLocation = new LatLng(pickupLocationLat, pickupLocationLong);
        return this.pickupLocation;
    }

    LatLng getDeliveryLocation(){
        this.deliveryLocation = new LatLng(deliveryLocationLat, deliveryLocationLong);
        return this.deliveryLocation;
    }

    String getStatus(){return this.status;}

    public int getPickupNumber(){
        return this.pickupNumber;
    }

    public int getReleaseCourierNumber(){
        return this.releaseCourierNumber;
    }

    public String getCourierUid(){
        return this.courierUid;
    }

    public long getFirebaseId(){
        return this.firebaseId;
    }

    void set_pickupDate(Date newdate){
        this.pickupDate = newdate;
    }

    void set_deliveryDate(Date newdate){
        this.deliveryDate = newdate;
    }

    void set_pickupNumber(int new_pickupNumber){
        this.pickupNumber = new_pickupNumber;
    }

    void set_releaseCourierNumber(int new_rcn){
        this.releaseCourierNumber = new_rcn;
    }

    void set_pickupLocation(LatLng newlocation, String address){
        this.pickupLocation = newlocation;
        this.pickupLocationLong = this.pickupLocation.longitude;
        this.pickupLocationLat = this.pickupLocation.latitude;
        this.pickupAddress = address;
    }

    void set_deliveryLocation(LatLng newlocation, String address){
        this.deliveryLocation = newlocation;
        this.deliveryLocationLong = this.deliveryLocation.longitude;
        this.deliveryLocationLat = this.deliveryLocation.latitude;
        this.deliveryAddress = address;
    }

    void set_status(String newstatus){
        this.status = newstatus;
    }

    void set_courierUid(String uid){
        this.courierUid = uid;
    }

    void set_firebaseId(long id){
        this.firebaseId = id;
    }

    private void savein_firebaseDatabase(){
        if(firebaseId<=0){
            this.save_newPackageInFirebase();
        }
        else{
            this.update_packageInFirebase();
        }
    }

    /** @brief Saves a new Package or updates a Package in the Firebase Database. */
    private void update_packageInFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference packages_db = database.getReference("Packages");
        //SaveUpdate PACKAGE IN FIREBASE DATABASE:
        String package_id = "package-"+packageName+"-"+new Long(firebaseId).toString();
        packages_db.child(package_id).child("package_name").setValue(packageName);
        packages_db.child(package_id).child("id").setValue(firebaseId);
        packages_db.child(package_id).child("status").setValue(status);
        packages_db.child(package_id).child("pickup_datetime").setValue(pickupDate);
        packages_db.child(package_id).child("delivery_datetime").setValue(deliveryDate);
        packages_db.child(package_id).child("pickup_location").setValue(pickupLocation);
        packages_db.child(package_id).child("pickup_address").setValue(pickupAddress);
        packages_db.child(package_id).child("delivery_location").setValue(deliveryLocation);
        packages_db.child(package_id).child("delivery_address").setValue(deliveryAddress);
        packages_db.child(package_id).child("courier_release_number").setValue(releaseCourierNumber);
        packages_db.child(package_id).child("pickup_number").setValue(pickupNumber);
        packages_db.child(package_id).child("courier_uid").setValue(courierUid);
        this.save(false);
    }

    /** @brief Tries to get an id of the firebase database to identify the package. If this fails
     * (because an internet connection is missing) an local id has to generated until the package
     * can be registered in firebase */
    private void save_newPackageInFirebase(){
        //READ PACKAGE DATABASE AND FIND THE LOWEST UNUSED PACKAGE ID:
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference packages_db = database.getReference("Packages");
        packages_db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long min_id = 0;
                long current_id;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    current_id = (long) snapshot.child("id").getValue();
                    if(current_id>min_id){
                        min_id = current_id;
                    }
                }
                firebaseId = min_id+1;
                //SAVE PACKAGE IN FIREBASE DATABASE:
                update_packageInFirebase();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public String toString() {
        return getPackageName()+" - "+getStatus();
    }

    public boolean save(boolean inFirebase){
        //Implement save also in Firebase
        if(inFirebase) {
            this.savein_firebaseDatabase();
        }
        return super.save();
    }

}


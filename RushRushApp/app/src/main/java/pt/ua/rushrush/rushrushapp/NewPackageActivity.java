package pt.ua.rushrush.rushrushapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class NewPackageActivity extends AppCompatActivity {
    final static int SIGNAL_PICKUP_TIME_LISTENER = 996;
    final static int SIGNAL_PICKUP_DATE_LISTENER = 997;
    final static int SIGNAL_DELIVERY_TIME_LISTENER = 998;
    final static int SIGNAL_DELIVERY_DATE_LISTENER = 999;
    final static int PICKUP_LOCATION = 1;
    final static int DELIVERY_LOCATION = 2;

    private EditText editText_packageName;
    private EditText editText_pickupDate;
    private EditText editText_pickupTime;
    private EditText editText_deliveryDate;
    private EditText editText_deliveryTime;
    private Button button_setPickupLocation;
    private Button button_setDeliveryLocation;
    private TextView textView_pickupLocation;
    private TextView textView_deliveryLocation;

    private Calendar calendar;
    private String format;

    private DatePicker PickupDatePicker;
    private int pickupYear, pickupMonth, pickupDay;
    private final int REQUEST_CODE_PLACEPICKER_PICKUP = 1;
    private TimePicker pickupTimePicker;
    private int pickupHour, pickupMinutes;
    private LatLng pickupLocation;
    private String pickupAddress;

    private DatePicker DeliveryDatePicker;
    private int deliveryYear, deliveryMonth, deliveryDay;
    private final int REQUEST_CODE_PLACEPICKER_DELIVERY = 2;
    private TimePicker deliveryTimePicker;
    private int deliveryHour, deliveryMinutes;
    private LatLng deliveryLocation;
    private String deliveryAddress;

    Random randomNumberGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_package);
        //getActionBar().setTitle("New Package");
        editText_packageName = (EditText) findViewById(R.id.editText_packageName);
        editText_pickupDate = (EditText) findViewById(R.id.editText_pickupData);
        editText_pickupTime = (EditText) findViewById(R.id.editText_pickupTime);
        textView_pickupLocation = (TextView) findViewById(R.id.textView_pickupLocation);
        button_setPickupLocation = (Button) findViewById(R.id.button_setPickupLocation);
        button_setPickupLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_placePickerActivity(PICKUP_LOCATION);
            }
        });
        editText_deliveryDate = (EditText) findViewById(R.id.editText_deliveryData);
        editText_deliveryTime = (EditText) findViewById(R.id.editText_deliveryTime);
        textView_deliveryLocation = (TextView) findViewById(R.id.textView_deliveryLocation);
        button_setDeliveryLocation = (Button) findViewById(R.id.button_setDeliveryLocation);
        button_setDeliveryLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_placePickerActivity(DELIVERY_LOCATION);
            }
        });
        //INIT OBJECT FOR DATABASE ACCESS:
        FlowManager.init(this);
        randomNumberGenerator = new Random();

        //SET INITIAL VALUE FOR THE PICKUP-/DELIVERY DATE:
        calendar = Calendar.getInstance();
        pickupYear = calendar.get(Calendar.YEAR);
        pickupMonth = calendar.get(Calendar.MONTH)+1;
        pickupDay = calendar.get(Calendar.DAY_OF_MONTH);
        set_pickupDate(pickupYear, pickupMonth, pickupDay);
        deliveryYear = pickupYear;
        deliveryMonth = pickupMonth;
        deliveryDay = pickupDay+1;
        set_deliveryDate(deliveryYear, deliveryMonth, deliveryDay);

        //SET INITIAL VALUE FOR DELIVERY TIME:
        pickupHour = calendar.get(Calendar.HOUR_OF_DAY);
        pickupMinutes = calendar.get(Calendar.MINUTE);
        set_pickupTime(pickupHour, pickupMinutes);
        deliveryHour = pickupHour;
        deliveryMinutes = pickupMinutes;
        set_deliveryTime(deliveryHour, deliveryMinutes);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == SIGNAL_PICKUP_DATE_LISTENER) {
            return new DatePickerDialog(this, pickupDateListener, pickupYear, pickupMonth, pickupDay);
        }
        if(id == SIGNAL_PICKUP_TIME_LISTENER) {
            return new TimePickerDialog(this, pickupTimeListener, pickupHour, pickupMinutes, true);
        }
        if (id == SIGNAL_DELIVERY_DATE_LISTENER) {
            return new DatePickerDialog(this, deliveryDateListener, deliveryYear, deliveryMonth, deliveryDay);
        }
        if(id == SIGNAL_DELIVERY_TIME_LISTENER) {
            return new TimePickerDialog(this, deliveryTimeListener, deliveryHour, deliveryMinutes, true);
        }
        return null;
    }

    public void onClick_setPickupDate(View view) {
        showDialog(SIGNAL_PICKUP_DATE_LISTENER);
        Toast.makeText(getApplicationContext(), "Choose the pickup date.",
                Toast.LENGTH_SHORT)
                .show();
    }

    public void onClick_setPickupTime(View view) {
        showDialog(SIGNAL_PICKUP_TIME_LISTENER);
        Toast.makeText(getApplicationContext(), "Choose the pickup time.",
                Toast.LENGTH_SHORT)
                .show();
    }

    public void onClick_setDeliveryDate(View view) {
        showDialog(SIGNAL_DELIVERY_DATE_LISTENER);
        Toast.makeText(getApplicationContext(), "Choose the delivery date.",
                Toast.LENGTH_SHORT)
                .show();
    }

    public void onClick_setDeliveryTime(View view) {
        showDialog(SIGNAL_DELIVERY_TIME_LISTENER);
        Toast.makeText(getApplicationContext(), "Choose the delivery time.",
                Toast.LENGTH_SHORT)
                .show();
    }

    private DatePickerDialog.OnDateSetListener pickupDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    set_pickupDate(arg1, arg2+1, arg3);
                }
            };

    private TimePickerDialog.OnTimeSetListener pickupTimeListener = new
            TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker arg0,int arg1, int arg2) {
                    set_pickupTime(arg1, arg2);
                }
            };

    private DatePickerDialog.OnDateSetListener deliveryDateListener = new
            DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0,
                              int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            set_deliveryDate(arg1, arg2+1, arg3);
        }
    };

    private TimePickerDialog.OnTimeSetListener deliveryTimeListener = new
            TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker arg0,int arg1, int arg2) {
                    set_deliveryTime(arg1, arg2);
                }
            };

    private void start_placePickerActivity(int locationType) {
        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
        // this would only work if you have your Google Places API working

        try {
            Intent intent = intentBuilder.build(this);
            if(locationType==PICKUP_LOCATION) {
                startActivityForResult(intent, REQUEST_CODE_PLACEPICKER_PICKUP);
            }
            else{
                startActivityForResult(intent, REQUEST_CODE_PLACEPICKER_DELIVERY);
            }
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("@strings/APP_TAG", "GooglePlayServicesNotAvailable");
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e){
            Log.e("@strings/APP_TAG", "GooglePlayServicesRepairable");
            e.printStackTrace();
        }
    }

    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PLACEPICKER_PICKUP && resultCode == RESULT_OK) {
            set_pickupLocation(data);
        }
        if (requestCode == REQUEST_CODE_PLACEPICKER_DELIVERY && resultCode == RESULT_OK) {
            set_deliveryLocation(data);
        }
    }

    private void set_pickupLocation(Intent data) {
        Place placeSelected = PlacePicker.getPlace(data, this);

        String name = placeSelected.getName().toString();
        pickupAddress = placeSelected.getAddress().toString();
        pickupLocation = placeSelected.getLatLng();
        textView_pickupLocation.setText(name + ", " + pickupAddress);
    }

    private void set_deliveryLocation(Intent data) {
        Place placeSelected = PlacePicker.getPlace(data, this);

        String name = placeSelected.getName().toString();
        deliveryAddress = placeSelected.getAddress().toString();
        deliveryLocation = placeSelected.getLatLng();
        textView_deliveryLocation.setText(name + ", " + deliveryAddress);
    }

    private void set_pickupDate(int year, int month, int day) {
        editText_pickupDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void set_pickupTime(int hour, int min) {
        editText_pickupTime.setText(new StringBuilder().append(String.format("%02d", hour))
                .append(":").append(String.format("%02d", min)));
    }

    private void set_deliveryDate(int year, int month, int day) {
        editText_deliveryDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void set_deliveryTime(int hour, int min) {
        editText_deliveryTime.setText(new StringBuilder().append(String.format("%02d", hour))
                .append(":").append(String.format("%02d", min)));
    }

    public void onClick_OK(View view) {
        SimpleDateFormat df_date = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df_time = new SimpleDateFormat("hh:mm");

        //GET UI INFORMATION:
        String packageName = editText_packageName.getText().toString();
        String pickupDateString = editText_pickupDate.getText().toString();
        String pickupTimeString = editText_pickupTime.getText().toString();
        String deliveryDateString = editText_deliveryDate.getText().toString();
        String deliveryTimeString = editText_deliveryTime.getText().toString();

        //CONVERT UI INFORMATION:
        Date pickupDate = null;
        Date deliveryDate = null;
        Date tmpDate = null;
        try {
            pickupDate = df_date.parse(pickupDateString);
            tmpDate = df_time.parse(pickupTimeString);
            pickupDate.setHours(tmpDate.getHours());
            pickupDate.setMinutes(tmpDate.getMinutes());
            deliveryDate = df_date.parse(deliveryDateString);
            tmpDate = df_time.parse(deliveryTimeString);
            deliveryDate.setHours(tmpDate.getHours());
            deliveryDate.setMinutes(tmpDate.getMinutes());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //CREATE NEW PACKAGE ORDER:
        PackageOrder newPackage = new PackageOrder(packageName);
        newPackage.set_deliveryDate(deliveryDate);
        newPackage.set_pickupDate(pickupDate);
        newPackage.set_pickupLocation(pickupLocation, pickupAddress);
        newPackage.set_deliveryLocation(deliveryLocation, deliveryAddress);
        newPackage.set_releaseCourierNumber(randomNumberGenerator.nextInt(10000)+1);
        newPackage.set_pickupNumber(0);
        newPackage.set_courierUid("");
        Toast.makeText(NewPackageActivity.this, "Order created", Toast.LENGTH_SHORT).show();
        newPackage.save(true);
        Log.v("@strings/APP_TAG", "New package saved. -> Go back to PackageOverview");
        Intent i = new Intent(NewPackageActivity.this, PackageOverviewActivity.class);
        startActivity(i);
    }

    public void onClick_cancel(View view) {
        Intent i = new Intent(NewPackageActivity.this, PackageOverviewActivity.class);
        startActivity(i);
    }
}

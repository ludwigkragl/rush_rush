package pt.ua.rushrush.rushrushapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luggi on 17.11.2017.
 */
/** Handle all the data which will be saved on the device*/
public class ContentProvider {
    public static final String RUSHRUSH_USER_PREFERENCES = "rushRush_user_preferences";
    private SharedPreferences user_settings;
    private SharedPreferences.Editor editor_userSettings;
    private FirebaseAuth mAuth;

    private String key_registered = "registered";
    private String key_userName = "username";
    private String key_userPassword = "user_password";
    private String key_userEmail = "user_email";
    private String key_courier = "courier";
    private String key_firebaseUid = "firebase_uid";

    ContentProvider(Context context) {
        user_settings = context.getSharedPreferences(RUSHRUSH_USER_PREFERENCES, Context.MODE_PRIVATE);
        editor_userSettings = user_settings.edit();
        mAuth = FirebaseAuth.getInstance();
    }

    //INDICATES THAT THE APP WAS ALREADY USED AND THAT THERE EXISTS USER DATA IN THE SHARED PREFERENCES:
    public void set_registered(){
        editor_userSettings.putBoolean(key_registered, true);
        editor_userSettings.commit();
    }

    public boolean get_registered(){
        return user_settings.getBoolean(key_registered, false);
    }

    /** @brief Save if user is courier or not (customer)*/
    public void set_courier(boolean courier){
        editor_userSettings.putBoolean(key_courier, courier);
        editor_userSettings.commit();
    }

    public boolean is_Courier(){
        return user_settings.getBoolean(key_courier, false);
    }

    public void set_userName(String username){
        editor_userSettings.putString(key_userName, username);
        editor_userSettings.commit();
    }

    public String get_userName(){
        return user_settings.getString(key_userName, "None");
    }

    public void set_firebaseUid(String uid){
        editor_userSettings.putString(key_firebaseUid, uid);
        editor_userSettings.commit();
    }

    public String getKey_firebaseUid(){
        return user_settings.getString(key_firebaseUid, "None");
    }

    public void set_userEmail(String email){
        editor_userSettings.putString(key_userEmail, email);
        editor_userSettings.commit();
    }

    public String get_userEmail(){
        return user_settings.getString(key_userEmail, "None");
    }

    public void set_userPassword(String password){
        editor_userSettings.putString(key_userPassword, password);
        editor_userSettings.commit();
    }

    public String get_userPassword(){
        return user_settings.getString(key_userPassword, "");
    }

    public ArrayList<PackageOrder> get_examplePackages(){
        return PackageOrder.get_examplePackageOrders();
    }

    /** @brief return all packages which are saved in the database*/
    public ArrayList<PackageOrder> get_packages(){
        Log.v("RushRushApp", "Try to read package history");
        List<PackageOrder> packages = new Select()
                .from(PackageOrder.class)
                .queryList();
        if(packages.size()==0){
            return get_examplePackages();
        }
        else{
            return (ArrayList<PackageOrder>) packages;
        }
    }

    /** @brief Takes all value of all new packages, which already existed before or has the status "ORDER CREATED"*/
    public void update_packagesForCourier(List<PackageOrder> new_packages){
        //GET LOCAL PACKAGES:
        List<PackageOrder> old_packages = new Select()
                .from(PackageOrder.class)
                .queryList();
        //SAVE THE NEW PACKAGES IN THE DATABASE:
        for (PackageOrder packageOrderNew: new_packages){
            for (PackageOrder packageOrderOld: old_packages){
                if(packageOrderNew.getCourierUid().equals(mAuth.getUid())
                        || packageOrderNew.getStatus().equals("Order created")){
                    packageOrderNew.save(false);
                }
                else{
                    packageOrderNew.delete();
                }
            }
        }
    }

    public void update_packagesForCustomer(List<PackageOrder> new_packages){
        for (PackageOrder packageOrder: new_packages){
            packageOrder.save(false);
        }
    }
}

package pt.ua.rushrush.rushrushapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.ArrayList;


public class PackageOverviewFragment extends Fragment{
    private ContentProvider contentProvider;
    private ArrayAdapter<PackageOrder> adapterPackageOrders;
    private ListView listView_packageOverview;
    private OnListItemSelectedListener mListener;
    private ArrayList<PackageOrder> packages;

    public interface OnListItemSelectedListener {
        public void onItemSelected(PackageOrder packageOrder);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentProvider = new ContentProvider(getContext());

        //INIT DATABASE OBJECT:
        FlowManager.init(getContext());

        //SETUP LIST HANDLING:
        packages = contentProvider.get_packages();
        adapterPackageOrders = new ArrayAdapter<PackageOrder>(getActivity(),
                android.R.layout.simple_list_item_activated_1, packages);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_package_overview, container, false);

        //SHOW LIST ELEMENTS:
        listView_packageOverview = (ListView) view.findViewById(R.id.listview_package_overview);
        listView_packageOverview.setAdapter(adapterPackageOrders); //Bind Adapter to listView

        //SETUP ON CLICK LISTENER:
        listView_packageOverview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View item,
                                    int position, long rowId) {
                // Retrieve item based on position
                PackageOrder packageOrder = adapterPackageOrders.getItem(position);
                // Fire selected listener event with item
                mListener.onItemSelected(packageOrder);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    /** @brief The fragment can be included in different activities. For the case that it is included
     * in
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnListItemSelectedListener) {
            mListener = (OnListItemSelectedListener) activity;
        } else {
            throw new ClassCastException(
                    activity.toString()
                            + " must implement ItemsListFragment.OnListItemSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}

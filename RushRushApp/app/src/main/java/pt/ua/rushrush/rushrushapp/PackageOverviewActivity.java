package pt.ua.rushrush.rushrushapp;

import android.content.Intent;
import android.support.constraint.solver.widgets.Snapshot;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PackageOverviewActivity extends FragmentActivity implements
        PackageOverviewFragment.OnListItemSelectedListener {
    private FloatingActionButton button_addPackageOrder;
    private FloatingActionButton button_updatePackages;
    private ContentProvider contentProvider;
    private FirebaseAuth mAuth;
    private ArrayList<PackageOrder> packageOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_overview);
        button_addPackageOrder = (FloatingActionButton) findViewById(R.id.button_addPackageOrder);
        button_updatePackages = (FloatingActionButton) findViewById(R.id.button_updatePackages);
        contentProvider = new ContentProvider(this);
        FlowManager.init(this);
        mAuth = FirebaseAuth.getInstance(); //get login status
        button_updatePackages.setImageResource(android.R.drawable.ic_popup_sync);
        button_addPackageOrder.setImageResource(android.R.drawable.ic_input_add);

        //UPDATE BUTTON LAYOUT DEPENDING ON USER TYPE:
        if(contentProvider.is_Courier()){
            button_addPackageOrder.setVisibility(View.GONE);
        }
    }

    public void onClick_addPackageOrder(View view) {
        Intent i = new Intent(PackageOverviewActivity.this, NewPackageActivity.class);
        startActivity(i);
    }

    @Override
    public void onItemSelected(PackageOrder packageOrder) {
        // For phone, launch detail activity using intent
        Intent i = new Intent(this, PackageDetailActivity.class);
        // Embed the serialized item
        i.putExtra(PackageOrder.INTENT_PACKAGE_KEY, packageOrder);
        // Start the activity
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Log.v("@strings/APP_TAG", "Logout user");
        mAuth.signOut();
        Intent i = new Intent(PackageOverviewActivity.this, StartActivity.class);
        startActivity(i);
    }

    public void onClick_updatePackages(View view) {
        if (contentProvider.is_Courier()) {
            //READ PACKAGES FROM FIREBASE:
            packageOrders = new ArrayList<>();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference packages_db = database.getReference("Packages");
            packages_db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String status = snapshot.child("status").getValue().toString();
                        String packageName = snapshot.child("package_name").getValue().toString();
                        int crn = snapshot.child("courier_release_number").getValue(Integer.class);
                        int pickupNumber = snapshot.child("pickup_number").getValue(Integer.class);
                        Date deliveryDate = snapshot.child("delivery_datetime").getValue(Date.class);
                        String deliveryAddress = snapshot.child("delivery_address").getValue().toString();
                        String courierUid = snapshot.child("courier_uid").getValue().toString();
                        long firebaseId = snapshot.child("id").getValue(long.class);
                        Date pickupDate = snapshot.child("pickup_datetime").getValue(Date.class);
                        String pickupAddress = snapshot.child("pickup_address").getValue().toString();
                        LatLng deliveryLocation = new LatLng(snapshot.child("delivery_location").child("latitude").getValue(double.class),
                                        snapshot.child("delivery_location").child("longitude").getValue(double.class));
                        LatLng pickupLocation = new LatLng(snapshot.child("pickup_location").child("latitude").getValue(double.class),
                                        snapshot.child("pickup_location").child("longitude").getValue(double.class));

                        PackageOrder packageOrder = new PackageOrder(packageName);
                        packageOrder.set_pickupDate(pickupDate);
                        packageOrder.set_deliveryDate(deliveryDate);
                        packageOrder.set_deliveryLocation(deliveryLocation, deliveryAddress);
                        packageOrder.set_pickupLocation(pickupLocation, pickupAddress);
                        packageOrder.set_firebaseId(firebaseId);
                        packageOrder.set_releaseCourierNumber(crn);
                        packageOrder.set_pickupNumber(pickupNumber);
                        packageOrder.set_courierUid(courierUid);
                        packageOrder.set_status(status);
                        packageOrders.add(packageOrder);
                    }
                    contentProvider.update_packagesForCourier((List<PackageOrder>) packageOrders);
                    Toast.makeText(PackageOverviewActivity.this, "Package Orders updated", Toast.LENGTH_SHORT).show();
                    recreate();
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Toast.makeText(PackageOverviewActivity.this, "Firebase is not responding", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            packageOrders = contentProvider.get_packages();
            final ArrayList<PackageOrder> packageOrdersUpdated = new ArrayList<PackageOrder>();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference packages_db = database.getReference("Packages");
            packages_db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (PackageOrder package_i : packageOrders) {
                        //PACKAGE HAS EITHER ID==0 OR THE FIREBASE ID IN THE LOCAL DATABASE -> CHECK BOTH CASES:
                        String package_id = "package-" + package_i.getPackageName() + "-" + new Long(package_i.getFirebaseId()).toString();
                        if (dataSnapshot.child(package_id).exists()){
                            String status = dataSnapshot.child(package_id).child("status").getValue().toString();
                            String packageName = dataSnapshot.child(package_id).child("package_name").getValue().toString();
                            int crn = dataSnapshot.child(package_id).child("courier_release_number").getValue(Integer.class);
                            int pickupNumber = dataSnapshot.child(package_id).child("pickup_number").getValue(Integer.class);
                            Date deliveryDate = dataSnapshot.child(package_id).child("delivery_datetime").getValue(Date.class);
                            String deliveryAddress = dataSnapshot.child(package_id).child("delivery_address").getValue().toString();
                            String courierUid = dataSnapshot.child(package_id).child("courier_uid").getValue().toString();
                            long firebaseId = dataSnapshot.child(package_id).child("id").getValue(long.class);
                            Date pickupDate = dataSnapshot.child(package_id).child("pickup_datetime").getValue(Date.class);
                            String pickupAddress = dataSnapshot.child(package_id).child("pickup_address").getValue().toString();
                            LatLng deliveryLocation = new LatLng(dataSnapshot.child(package_id).child("delivery_location").child("latitude").getValue(double.class),
                                    dataSnapshot.child(package_id).child("delivery_location").child("longitude").getValue(double.class));
                            LatLng pickupLocation = new LatLng(dataSnapshot.child(package_id).child("pickup_location").child("latitude").getValue(double.class),
                                    dataSnapshot.child(package_id).child("pickup_location").child("longitude").getValue(double.class));

                            PackageOrder packageOrder = new PackageOrder(packageName);
                            packageOrder.set_pickupDate(pickupDate);
                            packageOrder.set_deliveryDate(deliveryDate);
                            packageOrder.set_deliveryLocation(deliveryLocation, deliveryAddress);
                            packageOrder.set_pickupLocation(pickupLocation, pickupAddress);
                            packageOrder.set_firebaseId(firebaseId);
                            packageOrder.set_releaseCourierNumber(crn);
                            packageOrder.set_pickupNumber(pickupNumber);
                            packageOrder.set_courierUid(courierUid);
                            packageOrder.set_status(status);
                            packageOrdersUpdated.add(packageOrder);
                        }
                    }
                    contentProvider.update_packagesForCustomer((List<PackageOrder>) packageOrdersUpdated);
                    Toast.makeText(PackageOverviewActivity.this, "Package Orders updated", Toast.LENGTH_SHORT).show();
                    recreate();
                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}


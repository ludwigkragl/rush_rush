package pt.ua.rushrush.rushrushapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/** Login activity which asks for the basic user information.
 *
 * User information will be saved in shared preferences*/
public class RegisterActivity extends AppCompatActivity {
    private String purpose; /**!< Purpose of this activity: register or login*/
    private ContentProvider contentProvider;
    private Button button_register;
    private EditText editText_name, editText_email, editText_password;
    private Switch switch_courier;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        contentProvider = new ContentProvider(this);
        button_register = (Button) findViewById(R.id.button_register_registerScreen);
        editText_email = (EditText) findViewById(R.id.editText_email);
        editText_name = (EditText) findViewById(R.id.editText_name);
        editText_password = (EditText) findViewById(R.id.editText_password);
        switch_courier = (Switch) findViewById(R.id.switch_courier);

        //CUSTOMISE UI DEPENDING ON THE PURPOSE:
        Intent intent = getIntent();
        purpose = intent.getStringExtra(StartActivity.purpose_key);
        String test = purpose;
        if(purpose.equals("login")){
            switch_courier.setVisibility(View.GONE);
            button_register.setText("login");
        }
        if(contentProvider.get_registered()==true){
            editText_name.setText(contentProvider.get_userName());
            editText_email.setText(contentProvider.get_userEmail());
            editText_password.setText(contentProvider.get_userPassword());
        }

        mAuth = FirebaseAuth.getInstance(); //get login status
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser(); //check user state
                if(user!=null){
                    Intent intent = new Intent(RegisterActivity.this, PackageOverviewActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            }
        };
    }
    /** @todo Enable to login, if user is already registered but user is not saved in the shared preferences. */
    public void onClick_buttonREGISTER(View view) {
        if(purpose.equals("login")){
            login_user();
        }
        else if(purpose.equals("register")){
            register_user();
        }
    }

    /** @todo This method does not support the cange of the courier property.*/
    private void login_user() {
        final String name = editText_name.getText().toString();
        String password = editText_password.getText().toString();
        String email = editText_email.getText().toString();

        if( (name!="") && (password !="") && (email!="") ) {

            //OVERWRITE SAVED USER SETTINGS:
            contentProvider.set_registered();
            contentProvider.set_userName(name);
            contentProvider.set_userEmail(email);
            contentProvider.set_userPassword(password);

            mAuth.signInWithEmailAndPassword(contentProvider.get_userEmail(), contentProvider.get_userPassword()).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        Toast.makeText(RegisterActivity.this, "sign in error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void register_user(){
        final String name = editText_name.getText().toString();
        String password = editText_password.getText().toString();
        String email = editText_email.getText().toString();
        final boolean courier = switch_courier.isChecked();

        if( (name!="") && (password !="") && (email!="") ){
            contentProvider.set_registered();
            contentProvider.set_userName(name);
            contentProvider.set_userEmail(email);
            contentProvider.set_userPassword(password);
            contentProvider.set_courier(courier);

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(!task.isSuccessful()){
                        Toast.makeText(RegisterActivity.this, "Registration error", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //SET DATABASE INFORMATION OF THE USER:
                        String user_id = mAuth.getCurrentUser().getUid();
                        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("Users");
                        DatabaseReference current_user_db;
                        if(courier){
                            current_user_db = db.child("@strings/COURIER_KEY").child(user_id);
                        }
                        else{
                            current_user_db = db.child("Customers").child(user_id);
                        }
                        current_user_db.child("@strings/NAME_KEY").setValue(name);
                        current_user_db.child("position").setValue(new LatLng(0, 0));
                        current_user_db.child("@strings/STARS_KEY").setValue(0);
                        current_user_db.child("number_of_valuations").setValue(0);
                    }
                }
            });
        }
    }

    public void onClick_cancel(View view) {
        Intent i = new Intent(RegisterActivity.this, StartActivity.class);
        startActivity(i);
    }

    //ENABLE AND DISABLE FIREBASE AUTH LISTENER:
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAuthListener);
    }
}

package pt.ua.rushrush.rushrushapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SelectCourierListFragment extends Fragment {
    private ArrayAdapter<Courier> adapterCourier;
    private ListView listView_couriers;
    private OnListItemSelectedListener mListener;

    public interface OnListItemSelectedListener {
        public void onCourierSelected(Courier courier);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference couriers_db = database.getReference("Users").child("Courier");
        final ArrayList<Courier> couriers = new ArrayList<>();
        adapterCourier = new ArrayAdapter<Courier>(getActivity(),
                android.R.layout.simple_list_item_activated_1, couriers);
        //GET COURIERS DATABASE:
        couriers_db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //COLLECT COURIERS IN AN ARRAYLIST:
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String uid = snapshot.getKey();
                    long star = (long) snapshot.child("stars").getValue();
                    String name = snapshot.child("name").getValue().toString();
                    Courier courier = new Courier(name, uid, star);
                    couriers.add(courier);
                }
                adapterCourier.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_courier_list, container, false);
        listView_couriers = (ListView) view.findViewById(R.id.listView_couriers);
        listView_couriers.setAdapter(adapterCourier);

        listView_couriers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Courier courier = adapterCourier.getItem(position);
                mListener.onCourierSelected(courier);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnListItemSelectedListener) {
            mListener = (OnListItemSelectedListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}

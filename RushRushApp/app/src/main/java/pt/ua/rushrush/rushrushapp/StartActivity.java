package pt.ua.rushrush.rushrushapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/** Loading initial settings
 *      - Shows a message that the user has to wait.
 *      - Reads if the user is already loged in. If yes the Overview will be loaded, else the login screen.*/
public class StartActivity extends AppCompatActivity {
    public static String purpose_key = "purpose";
    private ContentProvider contentProvider;
    private Button button_login;
    //private Button button_register;
    private boolean registered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        button_login = (Button) findViewById(R.id.button_login);
    }

    public void onClick_login(View view) {
        Intent i = new Intent(StartActivity.this, RegisterActivity.class);
        i.putExtra(purpose_key, "login");
        startActivity(i);
    }

    public void onClick_register(View view) {
        Intent i = new Intent(StartActivity.this, RegisterActivity.class);
        i.putExtra(purpose_key, "register");
        startActivity(i);
    }
}

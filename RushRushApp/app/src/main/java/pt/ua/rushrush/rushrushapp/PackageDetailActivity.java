package pt.ua.rushrush.rushrushapp;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

public class PackageDetailActivity extends AppCompatActivity {
    PackageDetailFragment fragmentPackageDetail;
    ContentProvider contentProvider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_detail);

        //GET THE PACKAGE TO SHOW:
        // Fetch the item to display from bundle
        PackageOrder packageOrder = (PackageOrder) getIntent().getSerializableExtra(PackageOrder.INTENT_PACKAGE_KEY);
        contentProvider = new ContentProvider(getContext());
        if (savedInstanceState == null) {
            fragmentPackageDetail = PackageDetailFragment.newInstance(packageOrder, contentProvider.is_Courier());
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flDetailContainer, fragmentPackageDetail);
            ft.commit();
        }
    }

    //Passing onClick method to the fragment method.
    public void onClick_setVisibility(View view) {
        fragmentPackageDetail.onClick_setVisibility(view);
    }

    public void onClick_enterSecretNumber(View view) {
        fragmentPackageDetail.onClick_enterSeretNumber(view);
    }

    public void onClick_acceptOrder(View view) {
        fragmentPackageDetail.onClick_acceptOrder(view);
    }
}

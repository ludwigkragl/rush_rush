package pt.ua.rushrush.rushrushapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Random;

public class PackageDetailFragment extends Fragment implements OnMapReadyCallback{
    public static final String ARGS_ISCOURIER = "iscourier";
    private TableLayout table_security;
    private Button button_setVisibility;
    private Button button_enterSecretNumber;
    private Button button_acceptOrder;
    private TextView textView_showSecretNumber;
    private EditText editText_enterSecretNumber;
    private TextView textView_status;
    private PackageOrder packageOrder;
    private GoogleMap map;
    private MapView mapView;
    private View mView;
    private  CameraUpdate cameraUpdate;
    private ContentProvider contentProvider;
    private boolean isCourier;
    private Random randomNumberGenerator;
    private FirebaseAuth mAuth;

    public PackageDetailFragment() {
        // Required empty public constructor
    }

    public static PackageDetailFragment newInstance(PackageOrder packageOrder, boolean is_courier) {
        PackageDetailFragment fragment = new PackageDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(PackageOrder.INTENT_PACKAGE_KEY, packageOrder); //Meaning?
        args.putBoolean(ARGS_ISCOURIER, is_courier);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            packageOrder = (PackageOrder) getArguments().getSerializable(PackageOrder.INTENT_PACKAGE_KEY);
            isCourier = getArguments().getBoolean(ARGS_ISCOURIER);
        }
        randomNumberGenerator = new Random();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_package_detail, container, false);
        TextView tvPackageName = (TextView) view.findViewById(R.id.textView_packageName);
        TextView tvPickupDate = (TextView) view.findViewById(R.id.textView_pickupDate);
        TextView tvPickupAddress = (TextView) view.findViewById(R.id.textView_pickupAddress);
        TextView tvDeliveryDate = (TextView) view.findViewById(R.id.textView_deliveryDate);
        TextView tvDeliveryAddress = (TextView) view.findViewById(R.id.textView_deliveryAddress);
        textView_status = (TextView) view.findViewById(R.id.tetView_status);
        mapView = (MapView) view.findViewById(R.id.mapView_pickupDeliveryMap);
        button_enterSecretNumber = (Button) view.findViewById(R.id.button_enterSecretNumber);
        button_setVisibility = (Button) view.findViewById(R.id.button_setVisibility);
        button_acceptOrder = (Button) view.findViewById(R.id.button_acceptOrder);
        textView_showSecretNumber = (TextView) view.findViewById(R.id.textView_showSecretNumber);
        editText_enterSecretNumber = (EditText) view.findViewById(R.id.editText_enterSecretNumber);
        table_security = (TableLayout) view.findViewById(R.id.table_security);

        Format formatter = new SimpleDateFormat(" HH:mm dd.MM.yyyy");
        tvPackageName.setText(packageOrder.getPackageName());
        tvPickupDate.setText(formatter.format(packageOrder.getPickupDate()));
        tvDeliveryAddress.setText(packageOrder.getDeliveryAddress());
        tvDeliveryDate.setText(formatter.format(packageOrder.getDeliverDate()));
        tvPickupAddress.setText(packageOrder.getPickupAddress());
        textView_status.setText(packageOrder.getStatus());

        //CUSTOMIZE VIEW DEPENDING ON THE SITUATION:
        if(!isCourier){
            textView_showSecretNumber.setText("Here is hiding your delivery number");
            button_acceptOrder.setVisibility(View.GONE);

            if(packageOrder.getStatus().equals("Order created")){
                table_security.setVisibility(View.GONE);
            }
            if(packageOrder.getStatus().equals("Courier found")){
                editText_enterSecretNumber.setHint("Enter pickup number.");
                textView_showSecretNumber.setText("Here is hiding your delivery number");
            }
            if(packageOrder.getStatus().equals("Package picked up")){
                editText_enterSecretNumber.setVisibility(View.GONE);
                button_enterSecretNumber.setVisibility(View.GONE);
            }
            if(packageOrder.getStatus().equals("Package delivered")){
                table_security.setVisibility(View.GONE);
            }
        }
        if(isCourier){
            textView_showSecretNumber.setText("Here is hiding your pickup number");
            if(packageOrder.getStatus().equals("Order created")) {
                table_security.setVisibility(View.GONE);
            }
            if(packageOrder.getStatus().equals("Courier found")){
                button_acceptOrder.setVisibility(View.GONE);
                button_enterSecretNumber.setVisibility(View.GONE);
                editText_enterSecretNumber.setVisibility(View.GONE);
            }
            if(packageOrder.getStatus().equals("Package picked up")){
                button_setVisibility.setVisibility(View.GONE);
                textView_showSecretNumber.setVisibility(View.GONE);
                button_acceptOrder.setVisibility(View.GONE);
                button_enterSecretNumber.setHint("Enter delivery number.");
            }
            if(packageOrder.getStatus().equals("Package delivered")){
                table_security.setVisibility(View.GONE);
                button_acceptOrder.setVisibility(View.GONE);
            }
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceSate){
        super.onViewCreated(view, savedInstanceSate);
        Log.w("@string/APP_TAG", "Map: onViewCreated.");
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        map = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.addMarker(new MarkerOptions().position(packageOrder.getPickupLocation()).title("Pickup Location").snippet(packageOrder.getPickupAddress()));
        googleMap.addMarker(new MarkerOptions().position(packageOrder.getDeliveryLocation()).title("Delivery Location").snippet(packageOrder.getDeliveryAddress()));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(packageOrder.getPickupLocation());
        builder.include(packageOrder.getDeliveryLocation());
        LatLngBounds bounds = builder.build();
        int padding = 150; // offset from edges of the map in pixels
        cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        //googleMap.moveCamera(cameraUpdate);
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {
                // Move camera.
                map.moveCamera(cameraUpdate);
                // Remove listener to prevent position reset on camera move.
                map.setOnCameraChangeListener(null);
            }
        });
    }

    public void onClick_setVisibility(View view) {
        if(!isCourier) {
            if (button_setVisibility.getText().equals("invisible")) {
                button_setVisibility.setText("visible");
                textView_showSecretNumber.setText(new Integer(packageOrder.getReleaseCourierNumber()).toString());
            } else {
                button_setVisibility.setText("invisible");
                textView_showSecretNumber.setText("Here is hiding your delivery number");
            }
        }
        else{
            if (button_setVisibility.getText().equals("invisible")) {
                button_setVisibility.setText("visible");
                textView_showSecretNumber.setText(new Integer(packageOrder.getPickupNumber()).toString());
            } else {
                button_setVisibility.setText("invisible");
                textView_showSecretNumber.setText("Here is hiding your pickup number");
            }
        }
    }

    public void onClick_enterSeretNumber(View view) {
        if(!isCourier && packageOrder.getStatus().equals("Courier found")){
            int secret_number = Integer.parseInt(editText_enterSecretNumber.getText().toString());
            if(secret_number == packageOrder.getPickupNumber()){
                textView_status.setText("Package picked up");
                packageOrder.set_status("Package picked up");
                Toast.makeText(getContext(), "Package picked up", Toast.LENGTH_SHORT).show();
                packageOrder.save(true);
            }
        }
        if(isCourier && packageOrder.getStatus().equals("Package picked up")){
            int secret_number = Integer.parseInt(editText_enterSecretNumber.getText().toString());
            if(secret_number == packageOrder.getReleaseCourierNumber()){
                textView_status.setText("Package delivered");
                packageOrder.set_status("Package delivered");
                Toast.makeText(getContext(), "Package delivered", Toast.LENGTH_SHORT).show();
                packageOrder.save(true);
            }
        }
    }

    public void onClick_acceptOrder(View view) {
        //CHANGE LOCAL STATUS:
        packageOrder.set_status("Courier found");
        packageOrder.set_pickupNumber(randomNumberGenerator.nextInt(10000)+1);
        packageOrder.set_courierUid(mAuth.getUid());
        Toast.makeText(getContext(), "Courier found", Toast.LENGTH_SHORT).show();
        packageOrder.save(true);
        textView_status.setText(packageOrder.getStatus());
    }
}

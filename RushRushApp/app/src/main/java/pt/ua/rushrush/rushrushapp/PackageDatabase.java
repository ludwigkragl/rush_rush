package pt.ua.rushrush.rushrushapp;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Luggi on 21.11.2017.
 */
@Database(name = PackageDatabase.NAME, version = PackageDatabase.VERSION)
public class PackageDatabase {
    public static final String NAME = "PackageOrderDataBase";
    public  static final int VERSION = 1;
}
